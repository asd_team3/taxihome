var app = angular.module('starter.controllers', ['starter.services']);


app.controller('BookingsCtrl', function ($scope, $ionicModal, $http, RequestsService, UpdateService, HandleBookingService, ModifyStatusService, PickupService, $ionicPopup, $timeout) {

    $scope.isPickupDisabled = true;
    $scope.isFinishDisabled = true;
    $scope.latitude = undefined;
    $scope.longitude = undefined;
    var directionsDisplay = undefined;
    var directionsService = undefined;
    navigator.geolocation.getCurrentPosition(function (position) {
        var latitude = position.coords.latitude;
        var longitude = position.coords.longitude;
        var mapProp = {
            center: {lat: latitude, lng: longitude},
            zoom: 14,
            mapTypeId: google.maps.MapTypeId.ROADMAP
        };
        map = new google.maps.Map(document.getElementById('map'), mapProp);
        $scope.marker = new google.maps.Marker(
          {map: map, position: mapProp.center});

        $scope.$apply(function () {
            $scope.latitude = latitude;
            $scope.longitude = longitude;
        });


        directionsDisplay = new google.maps.DirectionsRenderer({ draggable: true });
        directionsService = new google.maps.DirectionsService();
        directionsDisplay.setMap(map);
        directionsDisplay.setPanel(document.getElementById("directions"));
    });



    pushNotification = window.plugins.pushNotification;

    pushNotification.register(
        onNotification,
        errorHandler,
        {
            'badge': 'true',
            'sound': 'true',
            'alert': 'true',
            'ecb': 'onNotification',
            'senderID': '888192468988',
        }
    );

    var device_token = undefined;
    var booking_id = undefined;
    window.onNotification = function (e) {

        switch (e.event) {
            case 'registered':
                if (e.regid.length > 0) {

                    device_token = e.regid;
                    RequestsService.register(device_token).then(function (response) {
                        //console.log('registered!');
                    });

                    var f = function () {
                        UpdateService.update(device_token).then(function (response) {
                            //console.log('updated!');
                            $timeout(f, 20000);
                        });
                    };
                    f();
                }
                break;

            case 'message':
                booking_id =  e.payload.booking_id;
                var confirmPopup = $ionicPopup.confirm({
                    title: 'Order notification',
                    template: 'There is a booking from customer, booking id: ' + e.payload.booking_id,
                    cancelText: 'Reject',
                    okText: 'Accept'
                });
                confirmPopup.then(function (res) {
                    if (res) {
                        console.log('Accept clicked');

                        HandleBookingService.handle_booking(true, device_token, e.payload.booking_id).then(function (success) {
                            var curCoords = new google.maps.LatLng($scope.latitude, $scope.longitude);
                            var destCoords = new google.maps.LatLng($scope.latitude - 0.07, $scope.longitude + 0.06); //TODO real coordinates
                            var request = {
                                origin: curCoords,
                                destination: destCoords,
                                travelMode: google.maps.TravelMode.DRIVING
                            };
                            directionsService.route(request, function (response, status) {
                                if (status == google.maps.DirectionsStatus.OK) {
                                    directionsDisplay.setDirections(response);
                                }
                            })
                            $scope.isPickupDisabled = false;
                        });


                    } else {
                        console.log('Canceled');
                        HandleBookingService.handle_booking(false, device_token, e.payload.booking_id).then(function (response) {
                            //console.log('Confirmed');
                        });
                    }
                });

                break;


            case 'error':
                alert('error occured');
                break;

        }
    };

    $scope.pickup = function() {
        console.log('onPickup');

       // directionsDisplay.setDirections({ routes: [] });
        PickupService.pickup(booking_id).then(function (response) {
            console.log('Modified: picked up');
            $scope.isPickupDisabled = true;
            $scope.isFinishDisabled = false;
        });
    };

    $scope.finish = function() {
        console.log('onFinish');
        ModifyStatusService.modify_status().then(function (response) {
            console.log('Modified: finish');
            $scope.isFinishDisabled = true;
        });
    };


});

app.controller('AppCtrl', function ($scope, $ionicModal, $timeout, AuthService, $location) {

    // With the new view caching in Ionic, Controllers are only called
    // when they are recreated or on app start, instead of every page change.
    // To listen for when this page is active (for example, to refresh data),
    // listen for the $ionicView.enter event:
    //$scope.$on('$ionicView.enter', function(e) {
    //});

    // Form data for the login modal
    $scope.loginData = {};

    // Create the login modal that we will use later
    $ionicModal.fromTemplateUrl('templates/login/login.html', {
        scope: $scope
    }).then(function (modal) {
        $scope.modal = modal;
    });

    // Triggered in the login modal to close it
    $scope.closeLogin = function () {
        $scope.modal.hide();
    };

    // Open the login modal
    $scope.login = function () {
        $scope.modal.show();
    };

    // Perform the login action when the user submits the login form
    $scope.doLogin = function () {
        console.log('Doing login', $scope.loginData);
        AuthService.login($scope.loginData.username, $scope.loginData.password).then(function (response) {
            $location.path('/bookings/receive');
        });
    }


    window.errorHandler = function (error) {
        alert('an error occured aq ?');
    }
})










