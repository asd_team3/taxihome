/**
 * Created by kote on 15.12.15.
 */
(function(){

    angular.module('starter')
        .service('PickupService', ['$http', '$q', '$ionicLoading',  PickupService]);

    function PickupService($http, $q, $ionicLoading, $timeout){

        var base_url = 'http://limitless-retreat-9980.herokuapp.com';




        function pickup(booking_id){
            var deferred = $q.defer();



            $ionicLoading.show();



            $http.put(base_url + '/taxis/pickup/' + booking_id)
                .success(function(response){
                    $ionicLoading.hide();
                    console.log("pickup success")
                    deferred.resolve(response);

                })
                .error(function(data, status){
                    console.log("error status: " + status)
                    $ionicLoading.hide();
                    deferred.reject();
                });


            return deferred.promise;
        };


        return {
            pickup: pickup
        };
    }
})();
