(function(){

  angular.module('starter')
    .service('RequestsService', ['$http', '$q', '$ionicLoading',  RequestsService]);

  function RequestsService($http, $q, $ionicLoading, $timeout){

    var base_url = 'http://limitless-retreat-9980.herokuapp.com';

    function register(device_token){

      var deferred = $q.defer();


      $ionicLoading.show();

      $http.post(base_url + '/register_token', {'device_token': device_token})
        .success(function(response){
            console.log("Successfully registered");
            $ionicLoading.hide();
            deferred.resolve(response);

        })
        .error(function(data, status){
            console.log("error status: " + status)
            $ionicLoading.hide();
            deferred.reject();
        });


      return deferred.promise;

    };

    return {
      register: register
    };
  }
})();
