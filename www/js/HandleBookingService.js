/**
 * Created by kote on 14.12.15.
 */
(function(){

    angular.module('starter')
        .service('HandleBookingService', ['$http', '$q', '$ionicLoading',  HandleBookingService]);

    function HandleBookingService($http, $q, $ionicLoading, $timeout){

        var base_url = 'http://limitless-retreat-9980.herokuapp.com';




        function handle_booking(confirmed, device_token, booking_id){
            var deferred = $q.defer();


            console.log("device_token: " + device_token);
            var config = {
                headers : {
                    'Content-Type': 'application/x-www-form-urlencoded;charset=utf-8;'
                }
            }
            $ionicLoading.show();

            var data = $.param({

                "decision" : {
                    booking_id: booking_id,
                    confirmed: confirmed,
                    device_token: device_token
                }

            });

            $http.put(base_url + '/taxis/decision', data, config)
                .success(function(response){
                    $ionicLoading.hide();
                    console.log("confirmed")
                    deferred.resolve(response);

                })
                .error(function(data, status){
                    console.log("error status: " + status)
                    $ionicLoading.hide();
                    deferred.reject();
                });


            return deferred.promise;
        };


        return {
            handle_booking: handle_booking
        };
    }
})();
