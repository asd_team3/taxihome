/**
 * Created by kote on 14.12.15.
 */
(function(){

    angular.module('starter')
        .service('ModifyStatusService', ['$http', '$q', '$ionicLoading',  ModifyStatusService]);

    function ModifyStatusService($http, $q, $ionicLoading, $timeout){

        var base_url = 'http://limitless-retreat-9980.herokuapp.com';




        function modify_status(){
            var deferred = $q.defer();


            $ionicLoading.show();

            $http.put(base_url + '/taxis/make_available')
                .success(function(response){
                    $ionicLoading.hide();
                    console.log("status available");
                    deferred.resolve(response);

                })
                .error(function(data, status){
                    console.log("error status: " + status);
                    $ionicLoading.hide();
                    deferred.reject();
                });


            return deferred.promise;
        };


        return {
            modify_status: modify_status
        };
    }
})();
