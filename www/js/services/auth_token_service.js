/**
 * Created by kote on 8.12.15.
 */
'use strict';

var app = angular.module('starter.token', []);

app.service("AuthToken", function() {
  return {
    get: function() {
      return localStorage.getItem('token');
    },
    set: function(token) {
      return localStorage.setItem('token',token);
    }
  }
});
