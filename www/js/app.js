// Ionic Starter App

// angular.module is a global place for creating, registering and retrieving Angular modules
// 'starter' is the name of this angular module example (also set in a <body> attribute in index.html)
// the 2nd parameter is an array of 'requires'
// 'starter.controllers' is found in controllers.js
var app = angular.module('starter', ['ionic', 'starter.controllers', 'starter.interceptors']);


app.run(function ($ionicPlatform, RequestsService, $timeout) {
    $ionicPlatform.ready(function () {
        // Hide the accessory bar by default (remove this to show the accessory bar above the keyboard
        // for form inputs)

        if (window.cordova && window.cordova.plugins.Keyboard) {
            cordova.plugins.Keyboard.hideKeyboardAccessoryBar(true);
            cordova.plugins.Keyboard.disableScroll(true);

        }
        if (window.StatusBar) {
            // org.apache.cordova.statusbar required
            StatusBar.styleDefault();
        }

        pushNotification = window.plugins.pushNotification;

        pushNotification.register(
            onNotification,
            errorHandler,
            {
                'badge': 'true',
                'sound': 'true',
                'alert': 'true',
                'ecb': 'onNotification',
                'senderID': '888192468988',
            }
        );
    });
    window.onNotification = function (e) {
    }


});

app.config(function ($stateProvider, $urlRouterProvider) {
    $stateProvider
        .state('login', {
            url: '/login',
            abstract: true,
            templateUrl: 'templates/menu.html',
            controller: 'AppCtrl'
        })

        .state('login.login', {
            url: '/login',
            views: {
                'menuContent': {
                    templateUrl: 'templates/login/login.html'
                }
            }
        })

        .state('bookings', {
            url: '/bookings',
            abstract: true,
            templateUrl: 'templates/menu.html',
            controller: 'BookingsCtrl'
        })
        .state('bookings.receive', {
            url: '/receive',
            views: {
                'menuContent': {
                    templateUrl: 'templates/bookings/receive.html'
                }
            }
        });
    $urlRouterProvider.otherwise('/login/login');
});

app.config(function ($httpProvider) {
    return $httpProvider.interceptors.push("AuthInterceptor");
});


window.errorHandler = function (error) {
    alert('an error occured aq ?');
}
