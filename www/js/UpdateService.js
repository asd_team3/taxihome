/**
 * Created by kote on 14.12.15.
 */
(function(){

    angular.module('starter')
        .service('UpdateService', ['$http', '$q', '$ionicLoading',  UpdateService]);

    function UpdateService($http, $q, $ionicLoading, $timeout){

        var base_url = 'http://limitless-retreat-9980.herokuapp.com';


        function update(device_token){
            var deferred = $q.defer();



            console.log("Sending taxis coordinates...");
            navigator.geolocation.getCurrentPosition(function(position) {


                var latitude = position.coords.latitude;
                var longitude = position.coords.longitude;
                console.log("Current coordinates: " + latitude + ", " + longitude);



                var data = $.param({

                    "taxi_coords" : {
                        latitude: latitude,
                        longitude: longitude,
                        device_token: device_token
                    }

                });

                var config = {
                    headers : {
                        'Content-Type': 'application/x-www-form-urlencoded;charset=utf-8;'
                    }
                }

                $http.put('http://limitless-retreat-9980.herokuapp.com/taxis/update', data, config)
                    .success(function (response) {
                        console.log("successfully updated")
                        deferred.resolve(response);

                    })
                    .error(function (data, status, header, config) {
                        console.log("Error, Data: " + data +
                            "<hr />status: " + status +
                            "<hr />headers: " + header +
                            "<hr />config: " + config);
                        deferred.reject();
                    });

            });




            return deferred.promise;
        }


        return {
            update: update
        };
    }
})();
